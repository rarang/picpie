const gulp = require('gulp');
const gulpCli = require('gulp-cli');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const buffer = require('vinyl-buffer');
const csso = require('gulp-csso');
const imagemin = require('gulp-imagemin');
const merge = require('merge-stream');
const spritesmith = require('gulp.spritesmith');
const browserSync = require('browser-sync').create();

const sassOption = {
	errLogToConsole: true,
	outputStyle: 'expanded'
};

gulp.task('default', function() {
	console.log(1);
});

// watch
gulp.task('watch', function(){
	// browserSync.init({
    //     server: "../"
    // });

	gulp.watch('../src/scss/**/*.scss', ['sass-compile']);
	gulp.watch("../*.html").on('change', browserSync.reload);
	gulp.watch("../src/css/*.css").on('change', browserSync.reload);
});

// style
gulp.task('sass-compile', function(){
	return gulp.src(['../src/scss/**/*.scss'])
		.pipe(sourcemaps.init())
		.pipe(sass(sassOption).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('../src/css'));
});


// spritesmith
gulp.task('sprite', function () {
	// Generate our spritesheet
	let spriteData = gulp.src('../img/spr_origin/*.png').pipe(spritesmith({
		//imgName : css에 대한 스프라이트 url 경로
		imgName: '../../images/spr/sprite.png',
		cssName: '_sprite.scss',
		cssFormat: 'scss',
		padding: 20
	}));

	// Pipe image stream through image optimizer and onto disk
	let imgStream = spriteData.img
	// DEV: We must buffer our stream into a Buffer for `imagemin`
	.pipe(buffer())
	.pipe(imagemin())
	// 스프라이트 결과물 저장 경로
	.pipe(gulp.dest('../images/spr'));

	// Pipe CSS stream through CSS optimizer and onto disk
	let cssStream = spriteData.css
	// 스프라이트 SCSS 저장 경로
	.pipe(gulp.dest('../src/scss/references'));

	// Return a merged stream to handle both `end` events
	return merge(imgStream, cssStream)
});
