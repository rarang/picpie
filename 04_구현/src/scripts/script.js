// Write. Lee Yura


$(document).ready(function(){
	$('.feed-category-wrap .btn-category').html(
		$('.category-list').eq(0).html() + '<span class="arrow-down"></span>'
	);
	$('.slider-list-wrap').slick({
		variableWidth: true,
		adaptiveHeight: true,
		slidesToShow: 1,
		centerMode: true,
		dots: false
	});
	$('.thumb-draggable').draggable({
		axis:"x",
		scroll:true,
		stop : function(){
			if ($(this).width() < $(this).parent().width()) {
				$(this).css({
					left: 0
				})
			}
			else {
				if (parseInt($(this).css('left').split('px')[0]) >= 0) {
					$(this).css({
						left: 0
					})
				}
				if (parseInt($(this).css('left').split('px')[0]) <= -($(this).width() - $(this).parent().width())) {
					$(this).css({
						left: -($(this).width() - $(this).parent().width())
					})
				}
			}
		}
	});
	$('.feed-wrap').slick({
		adaptiveHeight: true
	});
	$('.feed-wrap').on('swipe', function(event, slick, direction){
		var index = $(this).slick('slickCurrentSlide');
		var length = $('.category-list').length;

		$('.feed-btn-wrap').remove();
		$('.feed-category-wrap .btn-category').html(
			$('.category-list').eq(index).html() + '<span class="arrow-down"></span>'
		);
		if(index == length) {
			$('.feed-category-wrap .btn-category').html(
				'<span class="add-category"></span>새 카테고리 추가<span class="arrow-down"></span>'
			);
			$('#layer-category').addClass('on');
			$('body').addClass('layer-scroll-block');
		}
	});
	$('.section-collection .link-feed').each(function(){
		$(this).contextmenu(function(e){
			e.preventDefault();
		})
		$(this).on('touchstart mousedown',function(e){
			var $wrap = $(this).closest('.feed-list-wrap');
			clearTimeout(this.downTimer);
		    this.downTimer = setTimeout(function() {
				$wrap.find('.feed-list').append(
					'<span class="feed-btn-wrap">'+
						'<button type="button" class="btn-collection btn-layer on" onclick="toggleLayer(this, \'on\')" data-layer="layer-remove-collection">'+
							'<i class="ico ico-favorite-check-on"></i>'+
							'<span class="blind">컬렉션에서 삭제하기</span>'+
						'</button>'+
					'</span>'
				);
		    }, 1000);
		}).on('touchend mouseup',function(e){
			clearTimeout(this.downTimer);
		})
	})
})

$('#btn-feed-like').on('click',function(){
	var hitNumArr = $(this).next('.btn-feed-hit-num').html().split(',');
	var hitNum = 0;

	for (var i=0; i<hitNumArr.length; i++) {
		hitNum += hitNumArr[i];
	}
	parseInt(hitNum);

	if(!$(this).hasClass('on')){
		$(this).addClass('on');
		hitNum++;
	}
	else {
		$(this).removeClass('on');
		hitNum--;
	}
	$(this).next('.btn-feed-hit-num').html(format(hitNum));
})
$('#btn-feed-favorite').on('click',function(){
	var hitNumArr = $(this).next('.btn-feed-hit-num').html().split(',');
	var hitNum = 0;
	var $thisObj = $(this);
	var $layer = $('#' + $(this).attr('data-layer'));

	for (var i=0; i<hitNumArr.length; i++) {
		hitNum += hitNumArr[i];
	}
	parseInt(hitNum);

	if(!$(this).hasClass('on')){
		$layer.find('.btn-form').on('click',function(){
			$thisObj.addClass('on');
			hitNum++;
			$thisObj.next('.btn-feed-hit-num').html(format(hitNum));
		})
	}
	else {
	}
})
$('#btn-remove-collection').on('click',function(){
	var hitNumArr = $('#btn-feed-favorite').next('.btn-feed-hit-num').html().split(',');
	var hitNum = 0;

	for (var i=0; i<hitNumArr.length; i++) {
		hitNum += hitNumArr[i];
	}
	parseInt(hitNum);

	if($('#btn-feed-favorite').hasClass('on')){
		$('#btn-feed-favorite').removeClass('on');
		hitNum--;
		$('#btn-feed-favorite').next('.btn-feed-hit-num').html(format(hitNum));
	}
})

$('.feed-category-list-wrap .link-category').on('click',function(){
	var index = $(this).parent().index();
	var $btn = $('.btn-accordion');
	var $obj = $('.btn-accordion').next('.accordion-content');

	$('.feed-category-wrap .btn-accordion').removeClass('on');
	$('.feed-wrap').slick('slickGoTo', index, true);
	pop($btn, $obj);

	$('.feed-category-wrap .btn-category').html($(this).html()+'<span class="arrow-down"></span>');
	return false;
})
$('.feed-category-wrap .btn-add-category').on('click', function(){
	var $btn = $('.btn-accordion');
	var $obj = $('.btn-accordion').next('.accordion-content');

	$('.feed-category-wrap .btn-accordion').removeClass('on');
	$('.feed-wrap').slick('slickGoTo', $('.category-list').length, true);
	pop($btn, $obj);
	$('.feed-category-wrap .btn-category').html(
		'<span class="add-category"></span>새 카테고리 추가<span class="arrow-down"></span>'
	);
})





// 공통부분
// select
$('.select-option').on('click',function(){
	var $optionCurrent = $(this).closest('.select-wrap').siblings('.select-current');
	$optionCurrent.find('.txt').text($(this).text());
	$optionCurrent.toggleClass('on');
})

// accordion 메뉴
$('.btn-accordion').on('click', function(){
	var $btn = $(this);
	var $obj = $(this).next('.accordion-content');

	toggle(this,'on');
	pop($btn, $obj);

	return false;
});

function pop(btn, content){
	if( btn.attr('aria-expanded') == 'false' ){
		btn.attr('aria-expanded', 'true');
		btn.attr('aria-pressed','true');
		content.attr('aria-hidden','false');
	}
	else if( btn.attr('aria-expanded') == 'true'){
		btn.attr('aria-expanded', 'false');
		btn.attr('aria-pressed','false');
		content.attr('aria-hidden','true');
	}
}
function toggle(obj, className) {
	if (typeof obj !== "object") {
		var obj = document.querySelector(obj);
	}
	obj.classList.toggle(className);
}
function toggleLayer(obj, className) {
	if (obj.getAttribute("data-layer")) {
		var $layer = $('#' + obj.getAttribute('data-layer'));
		$layer.addClass('on');
		$('body').addClass('layer-scroll-block');
		$layer.find('.btn-form').off('click');
		$layer.find('.btn-form').on('click', function (e) {
			if (obj.classList.contains(className)) {
				e.stopPropagation();
				obj.classList.remove(className);
			}
		})
	}
}
//https://stackoverflow.com/questions/3753483/javascript-thousand-separator-string-format/19840881
const format = num => {
	const n = String(num),
		p = n.indexOf('.')
	return n.replace(
		/\d(?=(?:\d{3})+(?:\.|$))/g,
		(m, i) => p < 0 || i < p ? `${m},` : m
	)
}