// Write. Lee Yura
var listNum = $('.carousel-list').length;
var $listContainer = $('.carousel-container');
var current = 1;

$('.carousel-next').on('click', function () {
    count('next');
});
$('.carousel-prev').on('click', function () {
    count('prev');
});
$('.carousel-container .num').on('click', function () {
    current = $('.num-list .num').index(this) + 1;
    $listContainer.attr('data-num', current);

    move('.carousel-list-wrap', current);
    return false;
});
$('.carousel-container').on('swiperight', function () {
    count('prev');
});
$('.carousel-container').on('swipeleft', function () {
    count('next');
});

function move(obj, num) {
    if ($listContainer.attr('data-num') == num) {
        $(obj).css({
            transform: 'translate(' + (num - 1) * -100 + '%)'
        })
        $('.carousel-container .num').removeClass('on');
        $('.carousel-container .num').eq(num - 1).addClass('on');
    }
}
function count(flag) {
    if (flag == 'next') {
        if (current == listNum)
            current = 1;
        else
            current++;
    }
    else {
        if (current == 1)
            current = listNum;
        else
            current--;
    }
    $listContainer.attr('data-num', current);
    move('.carousel-list-wrap', current);
}