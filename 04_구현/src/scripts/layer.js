// Write. Lee Yura

const $layerLink = document.getElementsByClassName('btn-layer');
const $layerFocus = document.getElementsByClassName('focus-layer');
const $layerDim = document.getElementsByClassName('layer-dim');
const $layerClose = document.getElementsByClassName('layer-close');
const $layerCancel = document.getElementsByClassName('layer-cancel');

// 레이어 열기
for (let i = 0; i < $layerLink.length; i++) {
    let toggle = false;

    $layerLink[i].addEventListener('click', function (e) {
        const $layer = document.getElementById(this.getAttribute('data-layer'));
        const $layer2 = document.getElementById(this.getAttribute('data-layer2'));

        if (this.getAttribute('data-layer2') == null )
            $layer.classList.add('on');
        else {
            if ($layer2.getAttribute('data-close') != 'false'){
                toggle = !toggle;
            }
            if (toggle) {
                $layer.classList.add('on');
            }
            else {
                $layer2.classList.add('on');
            }
        }
        document.body.classList.add('layer-scroll-block');
        e.preventDefault();
    })   
}
for (let i = 0; i < $layerFocus.length; i++) {
    $layerFocus[i].addEventListener('focus', function (e) {
        let $layer = document.getElementById(this.getAttribute('data-layer'));
        $layer.classList.add('on');
        document.body.classList.add('layer-scroll-block');
        e.preventDefault();
    })
}

//레이어 닫기
for (let i = 0; i < $layerDim.length; i++) {
    $layerDim[i].addEventListener('click', function () {
        this.closest('.layer').classList.remove('on');
        document.body.classList.remove('layer-scroll-block');
    })
}
for (let i = 0; i < $layerClose.length; i++) {
    $layerClose[i].addEventListener('click', function (e) {
        this.closest('.layer').classList.remove('on');
        document.body.classList.remove('layer-scroll-block');
        if(e.target.getAttribute('data-close') === 'false')
            this.closest('.layer').setAttribute('data-close', 'false');
        else
            this.closest('.layer').setAttribute('data-close', 'true');
    })
}